﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthPlayer : MonoBehaviour
{
    public float startingHealth = 250f; // Máu ban đầu của Player
    public float currentHealth; // Máu hiện tại của Player
    public Slider healthSlider; // Thanh máu hiển thị trên màn hình
    public Text presentText; // số lượng máu hiện tại dạng text
    public float PresentHealth; // số lượng máu hiện tại

    public float timeDie = 1.5f;
    public GameObject boom;

    public AudioSource audioSource;
    public AudioClip audioDefeat;
    public AudioSource audioSourcePlayerBoom;
    public AudioClip audioBoom;

    void Update()
    {
        //currentHealth = startingHealth; // Khởi tạo máu ban đầu
        healthSlider.value = currentHealth;
        presentText.text = PresentHealth.ToString();
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage; // Trừ máu
        PresentHealth -= damage;

        if (currentHealth <= 0 && PresentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        // Xóa enemy khỏi scene
        Destroy(gameObject);
        Instantiate(boom, transform.position, transform.rotation);

        OnOffUI();

        audioSource.clip = audioDefeat;
        audioSource.Play();
    }

    public GameObject gunCenter;
    public GameObject AvatarPlayer;
    public GameObject GunBullet;
    public GameObject PanelBg;
    public GameObject Defeat;

    void OnOffUI()
    {
        gunCenter.SetActive(false);
        AvatarPlayer.SetActive(false);
        GunBullet.SetActive(false);
        PanelBg.SetActive(true);
        Defeat.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsePotions : MonoBehaviour
{
    public int health = 5;
    public Text numberPotions; // số bình máu (text)
    public int NumberPotions; // số bình máu (int)

    public bool isPotions = false;

    public GameObject panelAnimation;

    public AudioSource audioSourcePlayer;
    public AudioClip audioPotions;

    public float oldTime = 0f;
    public float nextTime = 0.5f;

    void Start()
    {
        panelAnimation.SetActive(false);
    }

    public void FixedUpdate()
    {
        numberPotions.text = NumberPotions.ToString();
        // vì muốn dùng bình máu thì máu của hiện tại được cộng nên cần sử dụng các thuộc tính của HealthPlayer nên mới gọi lại ra
        HealthPlayer healthPlayer = GetComponent<HealthPlayer>();

        if (Time.time > oldTime + nextTime)
        {
            if (Input.GetKeyDown("q"))
            {
                // Nếu số lượng máu hiện tại <= máu lúc ban đầu - số máu được cộng khi dùng bình máu
                if (healthPlayer.PresentHealth <= healthPlayer.startingHealth - health)
                {
                    // kiểm tra xem có bình máu nào không, nếu còn thì dùng sẽ cộng máu và tăng chỉ số máu lên và trừ đi 1 bình cho mỗi lần dùng
                    if (isPotions == false)
                    {
                        healthPlayer.currentHealth += health;
                        healthPlayer.PresentHealth += health;
                        NumberPotions -= 1;
                        panelAnimation.SetActive(true);

                        audioSourcePlayer.clip = audioPotions;
                        audioSourcePlayer.Play();
                    }

                    if (NumberPotions > 0)
                    {
                        isPotions = false;
                    }

                    // nếu hết bình máu thì sẽ không dùng được nữa
                    if (NumberPotions <= 0)
                    {
                        isPotions = true;
                    }
                }

                // Nếu số lượng máu hiện tại >= máu lúc ban đầu - số máu được cộng khi dùng bình máu
                // và số lượng máu hiện tại < máu lúc ban đầu
                if (healthPlayer.PresentHealth >= healthPlayer.startingHealth - health && healthPlayer.PresentHealth < healthPlayer.startingHealth)
                {
                    // kiểm tra xem có bình máu nào không, nếu còn thì dùng sẽ cộng máu và tăng chỉ số máu lên và trừ đi 1 bình cho mỗi lần dùng
                    if (isPotions == false)
                    {
                        // Máu hiện tại += máu lúc ban đầu - số lượng máu hiện tại
                        healthPlayer.currentHealth += healthPlayer.startingHealth - healthPlayer.PresentHealth;
                        healthPlayer.PresentHealth += healthPlayer.startingHealth - healthPlayer.PresentHealth;
                        NumberPotions -= 1;
                        panelAnimation.SetActive(true);

                        audioSourcePlayer.clip = audioPotions;
                        audioSourcePlayer.Play();
                    }

                    if(NumberPotions > 0)
                    {
                        isPotions = false;
                    }

                    // nếu hết bình máu thì sẽ không dùng được nữa
                    if (NumberPotions <= 0)
                    {
                        isPotions = true;
                    }

                    if (healthPlayer.currentHealth == healthPlayer.startingHealth)
                    {
                        isPotions = true;
                    }
                }
            }

            if (Input.GetKeyUp("q"))
            {
                panelAnimation.SetActive(false);
            }

            oldTime = Time.time;
        }
    }

    public void ButtonPotions()
    {
        //healthPlayer.currentHealth += health;
    }
}

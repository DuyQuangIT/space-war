﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UFOMovement : MonoBehaviour
{
    public float speed = 5f;
    public float timeLoadScene = 1f;
    public GameObject startUFO;

    void Update()
    {
        UFOMove();
    }

    void UFOMove()
    {
        // MoveTowards() - hàm di chuyển từ đâu tới đâu
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, startUFO.transform.position, speed * Time.deltaTime);

        transform.LookAt(startUFO.transform.position); // nhìn theo startUFO
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "StartUFO")
        {
            Invoke("loadSceneIntroUFO2", timeLoadScene);
        }
    }

    void loadSceneIntroUFO2()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Victory : MonoBehaviour
{
    public GameObject[] enemys;
    public GameObject PanelBg;
    public GameObject victory;
    public float timevictory = 1.5f;
    public float timePanelBg = 0.8f;

    public AudioSource audioSource;
    public AudioClip audioVictory;

    void Update()
    {
        EnemyHealth enemyHealth = FindObjectOfType<EnemyHealth>();

        if(enemyHealth != null)
        {
            if (enemyHealth.currentHealth <= 0)
            {
                if (enemys.Length >= 0)
                {
                    // Xóa phần tử đầu tiên trong mảng
                    //Array.Copy(enemys, 1, enemys, 0, enemys.Length - 1);

                    // Giảm độ dài của mảng đi 1
                    Array.Resize(ref enemys, enemys.Length - 1);
                    Debug.Log(enemys.Length);

                    if (enemys.Length <= 0)
                    {
                        Debug.Log(enemys.Length);
                        Destroy(enemyHealth.gameObject); // hủy bỏ đối tượng được tìm thấy.

                        Invoke("PanelBackground", timePanelBg);
                        Invoke("VictoryUI", timevictory);
                    }
                }
            }
        }
    }

    void PanelBackground()
    {
        PanelBg.SetActive(true);

        audioSource.clip = audioVictory;
        audioSource.Play();
    }

    void VictoryUI()
    {
        victory.SetActive(true);
    }
}

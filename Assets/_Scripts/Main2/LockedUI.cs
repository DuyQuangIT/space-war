﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedUI : MonoBehaviour
{
    void Start()
    {
        Cursor.lockState = CursorLockMode.None; // mở khóa con trỏ

        // Hiển thị con trỏ chuột
        Cursor.visible = true;
    }
}

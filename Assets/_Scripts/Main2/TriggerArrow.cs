using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerArrow : MonoBehaviour
{
    public GameObject GlobeBase;
    public float speedAutoDown = 5f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "arrow")
        {
            Destroy(other.gameObject);
            GlobeBase.transform.Translate(Vector3.down * speedAutoDown * Time.deltaTime);
        }
    }
}

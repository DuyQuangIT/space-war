﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radar : MonoBehaviour
{
    public float speed = 10f;
    public float limit = 10f;
    private bool movingForward = true;

    void Update()
    {
        if (movingForward)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
            if (transform.position.z > limit)
            {
                movingForward = false;
                // để xoay vật thể, chúng ta sử dụng phương thức Quaternion.Euler()
                //transform.rotation = Quaternion.Euler(0f, -180f, 0f);
            }
        }
        else
        {
            transform.Translate(Vector3.back * speed * Time.deltaTime);
            if (transform.position.z < -limit)
            {
                movingForward = true;
                // để xoay vật thể, chúng ta sử dụng phương thức Quaternion.Euler()
                //transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            }
        }
    }
}

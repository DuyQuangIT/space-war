using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHairEnemy : MonoBehaviour
{
    public Transform GunBarrel;
    Ray ray;
    RaycastHit hitInfo;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ray.origin = GunBarrel.transform.position;
        ray.direction = GunBarrel.transform.forward;
        if (Physics.Raycast(ray, out hitInfo))
        {
            transform.position = hitInfo.point;
        }
        else
        {
            transform.position = ray.origin + ray.direction * 1000.0f;
        }
    }
}

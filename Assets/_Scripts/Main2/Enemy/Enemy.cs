﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float distanceRada = 30f;
    public float distanceRadaAttack = 20f;
    public Transform enemy; // vị trí hiện tại của enemy
    public Transform playerTarget; // vị trí hiện tại của player
    public float speedEnemy = 8f;

    public EnemyAI enemyAI;
    public StateMachine stateMachine;

    Animator animator;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        EnemyAttack();
    }

    void EnemyAttack()
    {
        // Kiểm tra xem đối tượng transform có tồn tại hay không
        if (transform != null && playerTarget != null)
        {
            if (Vector3.Distance(enemy.position, playerTarget.position) <= distanceRadaAttack)
            {
                animator.SetBool("isShoot", true);
                enemy.LookAt(playerTarget); // nhìn về hướng player
                enemyAI.enabled = false;
                stateMachine.enabled = false;

                if (FindObjectOfType<EnemyHealth>().currentHealth > 0f)
                {
                    FindObjectOfType<EnemyShooting>().Shoot();
                }
            }

            else
            {
                animator.SetBool("isShoot", false);
                enemyAI.enabled = true;
                stateMachine.enabled = true;
                //FindObjectOfType<MuzzleFlashShoot>().StopFiring();
                FindObjectOfType<WeaponEnemy>().StopFiring();
            }
        }
        
    }
}

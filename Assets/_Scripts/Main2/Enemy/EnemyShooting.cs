﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public Transform gunBarrel;

    public float shootingTime = 0.5f;
    float oldshootingTime = 0f; // ngay lúc bắn nó sẽ lưu giá trị này lại

    WeaponEnemy weaponEnemy;

    public AudioSource audioSourceEnemy;
    public AudioClip audioShoot;

    void Start()
    {
        weaponEnemy = GetComponentInChildren<WeaponEnemy>();
    }

    public void Shoot()
    {
        if(Time.time > oldshootingTime + shootingTime)
        {
            //FindObjectOfType<WeaponEnemy>().DrawHitEffect(); // ban đầu
            // Sửa
            WeaponEnemy weaponEnemy = FindObjectOfType<WeaponEnemy>();
            if (weaponEnemy != null)
            {
                weaponEnemy.DrawHitEffect();
            }

            weaponEnemy.StartFiring();
            oldshootingTime = Time.time;

            if (weaponEnemy.isFiring)
            {
                weaponEnemy.UpdateFiring(Time.deltaTime);
            }

            oldshootingTime = Time.time;

            audioSourceEnemy.clip = audioShoot;
            audioSourceEnemy.Play();
        }
    }
}

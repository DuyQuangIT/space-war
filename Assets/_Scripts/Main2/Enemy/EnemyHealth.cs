﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public float startingHealth = 150f; // Máu ban đầu của enemy
    public float currentHealth; // Máu hiện tại của enemy
    public Slider healthSlider; // Thanh máu hiển thị trên màn hình

    Animator animator;
    public GameObject effectDie;
    public float timeEffectDie = 0.5f; // sau 1 thời gian thì phát nổ

    public AudioSource audioSourceEnemyDie;
    public AudioSource audioSourceEnemyBoom;
    public AudioClip audioDie;
    public AudioClip audioBoom;

    private void Start()
    {
        currentHealth = startingHealth; // Khởi tạo máu ban đầu
        healthSlider.value = currentHealth; // Cập nhật giá trị thanh máu
        healthSlider.gameObject.SetActive(false);

        animator = gameObject.GetComponent<Animator>();
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage; // Trừ máu
        healthSlider.value = currentHealth; // Cập nhật giá trị thanh máu

        if (currentHealth <= 0)
        {
            Die(); // Nếu máu bằng 0 thì enemy chết
        }
    }

    private void Die()
    {
        // Xóa enemy khỏi scene
        healthSlider.gameObject.SetActive(false);

        animator.SetBool("isDie", true);
        //FindObjectOfType<Enemy>().enabled = false;
        //FindObjectOfType<StateMachine>().enabled = false;
        //FindObjectOfType<EnemyAI>().enabled = false;
        FindObjectOfType<EnemyShooting>().enabled = false;

        Invoke("EffectDie", timeEffectDie);

        audioSourceEnemyDie.clip = audioDie;
        audioSourceEnemyDie.Play();
    }

    public GameObject enemy;

    void EffectDie()
    {
        Instantiate(effectDie, enemy.transform.position, transform.rotation);
        Destroy(gameObject);

        audioSourceEnemyBoom.clip = audioBoom;
        audioSourceEnemyBoom.Play();
    }
}

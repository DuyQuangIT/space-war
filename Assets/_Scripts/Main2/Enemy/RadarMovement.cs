﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarMovement : MonoBehaviour
{
    public float speed = 100f; // Tốc độ di chuyển của radar
    public float rotationSpeed = 30f; // Tốc độ quay của radar
    public float xMin = -10f; // Khoảng cách tối thiểu di chuyển theo trục X
    public float xMax = 10f; // Khoảng cách tối đa di chuyển theo trục X
    public float yMin = -5f; // Khoảng cách tối thiểu di chuyển theo trục Y
    public float yMax = 5f; // Khoảng cách tối đa di chuyển theo trục Y

    void FixedUpdate()
    {
        // Tạo vị trí mới cho chiếc radar
        float newX = Random.Range(xMin, xMax);
        float newY = Random.Range(yMin, yMax);
        Vector3 newPosition = new Vector3(newX, newY, transform.position.z);

        // Di chuyển chiếc radar đến vị trí mới
        transform.position = Vector3.MoveTowards(transform.position, newPosition, speed * Time.deltaTime);

        // Tạo góc quay mới cho chiếc radar
        float newRotation = Random.Range(-180f, 180f);
        Quaternion targetRotation = Quaternion.Euler(0, 0, newRotation);

        // Quay chiếc radar đến góc mới
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }
}

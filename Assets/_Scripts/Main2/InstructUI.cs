using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructUI : MonoBehaviour
{
    public float timeHide = 2f;
    public GameObject instructUI;
    public GameObject gunCenterUI;

    void Update()
    {
        gunCenterUI.SetActive(false);
        Invoke("HideInstructUI", timeHide);
    }

    void HideInstructUI()
    {
        Destroy(instructUI);
        gunCenterUI.SetActive(true);
    }
}

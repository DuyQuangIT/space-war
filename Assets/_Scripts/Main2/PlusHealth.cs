﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusHealth : MonoBehaviour
{
    public AudioSource audioSourcePlayer;
    public AudioClip audioPlusHealth;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Health")
        {
            Destroy(other.gameObject);

            UsePotions usePotions = GetComponent<UsePotions>();
            if(usePotions.NumberPotions >= 0 && usePotions.NumberPotions <= 5)
            {
                usePotions.NumberPotions += 1;
            }

            audioSourcePlayer.clip = audioPlusHealth;
            audioSourcePlayer.Play();
        }
    }
}

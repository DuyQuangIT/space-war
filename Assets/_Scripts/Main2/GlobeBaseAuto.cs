﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobeBaseAuto : MonoBehaviour
{
    public float speed = 1.0f;

    private bool moveUp = true;

    public float yMax = 10.256f;
    public float yMin = 0.262f;

    void Update()
    {
        if (moveUp) // kiểm tra xem, nếu đúng nó đang chạy lên trên thì di chuyển lên trên
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
            if (transform.position.y >= yMax) // Nếu nó đạt đến giá trị max thì sẽ không cho true nữa, buộc phải false để nó di chuyển xuống
            {
                moveUp = false;
            }
        }
        else // Nếu không thì di chuyển xuống
        {
            transform.position -= Vector3.up * speed * Time.deltaTime;
            if (transform.position.y <= yMin) // Nếu nó đạt đến giá trị min thì sẽ không cho false nữa, buộc phải true để nó di chuyển lên
            {
                moveUp = true;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BarrelAnim : MonoBehaviour
{
    Animator animator;

    public GameObject barrel;
    public GameObject barrelAnim;

    public BoxCollider boxCollider;
    public float sizeY = 0.01f;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        boxCollider = gameObject.GetComponent<BoxCollider>();
    }

    void Update()
    {
        BarrelAnimation();

        SizeBoxCollider();
    }

    public void BarrelAnimation()
    {
        animator.SetBool("isScale", true);
    }

    public void SizeBoxCollider()
    {
        boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y + sizeY, boxCollider.size.z);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "floor")
        {
            animator.SetBool("isScale", false);
            barrel.SetActive(true);
            barrelAnim.SetActive(false);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}

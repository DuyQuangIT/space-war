﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastWeapon : MonoBehaviour
{
    public bool isFiring = false; // đơn giản chỉ để check xem đã được bắn hay chưa
    public int fireRate = 25;
    public ParticleSystem[] muzzleFlash;
    public ParticleSystem hitEffect;
    public TrailRenderer tracerEffect;

    public Transform raycastOrigin;
    public Transform raycastDestination;

    Ray ray;
    RaycastHit hitInfo;
    float accumulatedTime;

    public float damage = 10f;
    public float range = 18f;

    public GameObject magazine;

    public void StartFiring()
    {
        isFiring = true;
        accumulatedTime = 0.0f;
        FireBullet();
    }

    public void UpdateFiring(float deltaTime)
    {
        accumulatedTime += deltaTime;
        float fireInterval = 1.0f / fireRate;
        while(accumulatedTime >= 0.0f)
        {
            FireBullet();
            accumulatedTime -= fireInterval;
        }
    }

    private void FireBullet()
    {
        foreach (var particle in muzzleFlash)
        {
            particle.Emit(1);
        }

        ray.origin = raycastOrigin.position;
        ray.direction = raycastDestination.position - raycastOrigin.position;

        var tracer = Instantiate(tracerEffect, ray.origin, Quaternion.identity);
        tracer.AddPosition(ray.origin);

        if (Physics.Raycast(ray, out hitInfo, range))
        {
            DrawHitEffect(hitInfo);

            tracer.transform.position = hitInfo.point;

            if (hitInfo.collider.CompareTag("enemy")) // tìm đến nếu enemy có tag = "enemy"
            {
                // hitInfo.collider.GetComponent<EnemyHealth>() - đường raycast chạm vào vật thể có health (EnemyHealth)
                // Khai báo như vậy nhằm mục đích luôn cập nhật máu hiện tại của enemy khi bị bắn
                EnemyHealth enemyHealth = hitInfo.collider.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    enemyHealth.healthSlider.gameObject.SetActive(true);
                    enemyHealth.TakeDamage(damage); //Giảm damage (10f đã khai báo ở trên) điểm sức khỏe của enemy
                }
            }
        }
    }

    public void StopFiring()
    {
        isFiring = false;
    }

    public void DrawHitEffect(RaycastHit hitInfo)
    {
        hitEffect.transform.position = hitInfo.point;
        hitEffect.transform.forward = hitInfo.normal;
        hitEffect.Emit(1);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UICredits : MonoBehaviour
{
    public void CreditsRestart()
    {
        SceneManager.LoadScene("IntroUFO1");
    }

    public void CreditsQuit()
    {
        Application.Quit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defeat : MonoBehaviour
{
    public GameObject PanelBg;
    public GameObject defeat;
    public float timedefeat = 1.5f;
    public float timePanelBg = 0.8f;

    public GameObject gunCenter;
    public GameObject AvatarPlayer;
    public GameObject GunBullet;

    void Update()
    {
        HealthPlayer healthPlayer = FindObjectOfType<HealthPlayer>();

        if (healthPlayer != null)
        {
            if (healthPlayer.currentHealth <= 0 && healthPlayer.PresentHealth <= 0)
            {
                Invoke("PanelBackground", timePanelBg);
                Invoke("PanelBackground", timedefeat);
                Destroy(healthPlayer.gameObject); // hủy bỏ đối tượng được tìm thấy.
            }
        }
    }

    void PanelBackground()
    {
        PanelBg.SetActive(true);
    }

    void DefeatUI()
    {
        gunCenter.SetActive(false);
        AvatarPlayer.SetActive(false);
        GunBullet.SetActive(false);
        PanelBg.SetActive(true);
        defeat.SetActive(true);
    }
}

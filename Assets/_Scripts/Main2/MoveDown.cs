using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveDown : MonoBehaviour
{
    public float speedMoveDown = 5f;
    public float timeLoadScene = 1f;

    void FixedUpdate()
    {
        Invoke("moveDown", 1f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "floor")
        {
            speedMoveDown = 0f;

            Invoke("LoadScene", timeLoadScene);
        }
    }

    void moveDown()
    {
        transform.position += Vector3.down * speedMoveDown * Time.deltaTime;
    }

    void LoadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}

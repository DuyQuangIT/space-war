﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Defeat_Victory : MonoBehaviour
{
    public void Complete()
    {
        SceneManager.LoadScene("UIStart");
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        Cursor.lockState = CursorLockMode.None; // mở khóa con trỏ

        // Hiển thị con trỏ chuột
        Cursor.visible = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    RaycastWeapon weapon;

    public float oldTime;
    public float nextTime = 0.2f;

    public Text currentNumberBullet; // số đạn hiện tại (text)
    public int CurrentNumberBullet; // số đạn hiện tại (int)

    public AudioSource audioSourcePlayer;
    public AudioClip audioShoot;

    void Start()
    {
        weapon = GetComponentInChildren<RaycastWeapon>();
    }

    void Update()
    {
        currentNumberBullet.text = CurrentNumberBullet.ToString();

        if (Input.GetButton("Fire1") && Input.GetButton("Fire2"))
        {
            if(Time.time > oldTime + nextTime)
            {
                weapon.StartFiring();
                //FindObjectOfType<Gun>().Shoot();
                oldTime = Time.time;

                //weapon.UpdateFiring(Time.deltaTime);
                
                if (weapon.isFiring)
                {
                    weapon.UpdateFiring(Time.deltaTime);
                }

                CurrentNumberBullet -= 1;
                if(CurrentNumberBullet <= 0)
                {
                    CurrentNumberBullet = 30;
                }

                audioSourcePlayer.clip = audioShoot;
                audioSourcePlayer.Play();
            }
        }

        if (Input.GetButtonUp("Fire1"))
        {
            weapon.StopFiring();
        }
    }
}

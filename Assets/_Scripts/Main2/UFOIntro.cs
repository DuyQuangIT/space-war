﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UFOIntro : MonoBehaviour
{
    public float speed = 5f;
    public Vector3 offset;

    public float timeLoadScene = 1f;

    public GameObject cameraEndUFO;

    private void Update()
    {
        UFOMove();
    }

    void UFOMove()
    {
        // MoveTowards() - hàm di chuyển từ đâu tới đâu
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, cameraEndUFO.transform.position + offset, speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EndUFO")
        {
            Invoke("loadScene", timeLoadScene);
        }
    }

    void loadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}

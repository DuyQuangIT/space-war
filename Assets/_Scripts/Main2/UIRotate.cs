using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRotate : MonoBehaviour
{
    public float speedRotate = 10f;

    void Update()
    {
        transform.Rotate(new Vector3(0, 0, Time.deltaTime * speedRotate));
    }
}
